import re
import csv
import enchant
import os
import time
from stemming.porter2 import stem


#input: tokenized list of words
#output: count of bad words and list of indices of bad word
def return_badwords(line):
	
	list_words=[]
	count=0
	f = open("badwords_full.txt")
	bad_words = f.read().splitlines()
	d = enchant.Dict("en_US")
	d1 = enchant.Dict("en_GB")
	bad=enchant.request_pwl_dict("badwords_full.txt")
	d2=enchant.request_pwl_dict("adjectives.txt")
	d3=enchant.request_pwl_dict("interject.txt")

	for i in range(0,len(line)):
		if(len(line[i])>1):
			w=re.sub(r'([^\w\s]|_)+(?=\s|$)', '', line[i])
			if(len(w)>1):
				if (d2.check(w)==False and d3.check(w)==False):
			
			
					if(d.check(w) or d1.check(w)):
						if(w in bad_words):
							
							list_words.append(i)
							count=count+1
				
					else:	
						suggest=d.suggest(w);		
						for word in suggest:
							if(len(word)==w):
								if(word in bad_words):			
									list_words.append(i)
									count=count+1
									break
	
	return count,list_words

####################################################################
#Function to find the occurences of bad words
#inputs: tokenized list of the sentence (in order)
#output: returns list of indexes where bad_words occurs 
def bad_find(line):
	#intial work 
	found_list=[];
	badlist = os.path.join(os.path.dirname(__file__), "badwords_full.txt")
	f = open(badlist);
	keys = f.read().lower().splitlines();
	d = enchant.DictWithPWL("en_US",badlist);
	#d = enchant.request_pwl_dict(ulist); # to speed up 
	pcount=0;

	for i in range(len(line)):
		bw=re.sub(r'([^\w\s]|_)+(?=\s|$)', '', line[i])
		bw=bw.lower();

		 # add suggestes list wordn leng >= to original
		if bw in keys:
			found_list.append(i);
			pcount+=1;
			#print line[i]
		elif stem(bw) in keys:
			found_list.append(i);
			pcount+=1;

		elif not d.check(bw):
			sug_list = d.suggest(bw);
			sug_list=sug_list[0:5];   #insure that we dont pick really low porbabilty spell sugestions
			for w in sug_list:
				if w in keys:
					found_list.append(i);
					pcount+= 1#1.0/(1.0+sug_list.index(w));
					#print line[i]
					#print sug_list
					#print d.suggest(line[i]) 
					break;  #can only occur once
				elif stem(w) in keys:
					found_list.append(i);
					pcount+= 1#1.0/(1.0+sug_list.index(w));
					break;


	return pcount,found_list;	

############################## Not needed ####################################################
def check_all_combinations(bad_words,word,d,d1):

	for i in range(0,len(bad_words)):
		if(d.check(bad_words[i])==False and d1.check(bad_words[i])==False):
			if(len(bad_words[i])==len(word)):
				suggest=d.suggest(bad_words[i])
				
				if(word in suggest):
					print "suggest in function: ",suggest
					print "BAD WORD: ",bad_words[i]
					
					return 1
					break		
	
	return 0
############################################################################################
#test=['ekto', 'shitty', 'fuck', '_', 'you', 'and', 'ther', 'rest', 'of', 'your', 'faggot', 'friends', 'should', 'be', 'burned', 'at', 'the', 'stake']
test=['retardedy','loser','morons','shitting','retarded','retarted']
#test=['christonastickjust', 'now', 'there', 'is', 'no', 'free', 'speech', 'here', 'who', 'prevented', 'you', 'from', 'posting', 'this?']

print test
print return_badwords(test)
print bad_find(test)
#print stem('scumbag')
#d = enchant.DictWithPWL("en_US","badwords_full.txt");
#print d.check("retardedy")

iter=20;

t1=time.time();
for i in range(iter):
	return_badwords(test);

t2=time.time();
for i in range(iter):
	bad_find(test)

t3=time.time();

print "return_badwords():"
print (t2-t1)
print "bad_find():"	
print (t3-t2)









