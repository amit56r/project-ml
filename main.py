from classifier import svm, logreg, adaboost, randomforest
from preprocess import get_data,write_feature_set,get_joined
from transforms import stack_feature
from classifier.klog import Klog
from numpy  import *
from scipy.sparse import *
from scipy import *
from sklearn.feature_selection import SelectKBest, chi2

###########################################
#testing area
########################################

labels,fvec,words=get_data('train_init.csv')
t_labels,t_fvec,t_words=get_data('test_init.csv')

fvec,t_fvec=stack_feature(fvec,t_fvec,words,t_words)

print fvec[0]


#Feature Selection
selector  = SelectKBest(chi2, k=2000)
selector.fit(fvec,labels);
fvec=selector.transform(fvec);
t_fvec=selector.transform(t_fvec)



#print 'SVM'
#s_acc,s_auc,s_precision,s_recall,s_fbeta_score=svm.test(labels,fvec,t_labels,t_fvec);
print 'log_reg'
l_acc,l_auc,l_precision,l_recall,l_fbeta_score=logreg.test(labels,fvec,t_labels,t_fvec);
#print 'adaboost'
#a_acc,a_auc,a_precision,a_recall,a_fbeta_score=adaboost.test(labels, fvec, t_labels, t_fvec);
#print 'random forest'
#r_acc,r_auc,r_precision,r_recall,r_fbeta_score=randomforest.test(labels, fvec, t_labels, t_fvec);

#print 'cluster log'
#klog = Klog(20)
#k_acc,k_auc,k_precision,k_recall,k_fbeta_score=klog.test(labels,fvec,t_labels,t_fvec,type='deep');

#print '----SVM----'
#print("score: %f (ACC)" % s_acc)
#print("score: %f (AUC)" % s_auc)
#print("precision: %f" % s_precision)
#print("recall: %f" % s_recall)
#print("F-beta score: %f" % s_fbeta_score)

print '----log_reg----'
print("score: %f (ACC)" % l_acc)
print("score: %f (AUC)" % l_auc)
print("precision: %f" % l_precision)
print("recall: %f" % l_recall)
print("F-beta score: %f" % l_fbeta_score)

#print'----adaboost----'
#print("score: %f (ACC)" % a_acc)
#print("score: %f (AUC)" % a_auc)
#print("precision: %f" % a_precision)
#print("recall: %f" % a_recall)
#print("F-beta score: %f" % a_fbeta_score)

#print '----Random Forest-----'
#print("score: %f (ACC)" % r_acc)
#print("score: %f (AUC)" % r_auc)
#print("precision: %f" % r_precision)
#print("recall: %f" % r_recall)
#print("F-beta score: %f" % r_fbeta_score)

#print '----Cluster Log-----'
#print("score: %f (ACC)" % k_acc)
#print("score: %f (AUC)" % k_auc)
#print("precision: %f" % k_precision)
#print("recall: %f" % k_recall)
#print("F-beta score: %f" % k_fbeta_score)
