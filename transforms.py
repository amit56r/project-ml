from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from preprocess import get_joined
from numpy  import *
from scipy.sparse import *


#input: corpus of train and test data (no preprocess)
def apply_Tfidf(data,target):
	vectorizer = TfidfVectorizer(min_df=3,ngram_range=(1,3))
	fvec = vectorizer.fit_transform(data);
	test_fvec = vectorizer.transform(target);

	return fvec,test_fvec;

#input: corpus of train and test data (no preprocess)
def apply_count(data,target):
	vectorizer = CountVectorizer(min_df=3,ngram_range=(1,3))
	fvec = vectorizer.fit_transform(data);
	test_fvec = vectorizer.transform(target);

	return fvec,test_fvec;

#function to stack handcrafted feature to standard feature extraction 
def stack_feature(fvec,t_fvec,words,t_words):
	data = get_joined(words);
	target = get_joined(t_words);

	s_fvec,s_t_fvec = apply_Tfidf(data,target);
	#c_fvec,c_t_fvec = apply_count(data,target);


	fvec = csr_matrix(fvec);
	t_fvec = csr_matrix(t_fvec);

	fvec = hstack((fvec,s_fvec)).tocsr();
	t_fvec = hstack((t_fvec,s_t_fvec)).tocsr();

	return fvec, t_fvec

