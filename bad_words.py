import re
import csv
import enchant

#input: tokenized list of words
#output: count of bad words and list of indices of bad word
def return_badwords(line):
	
	list_words=[]
	count=0
	f = open("badwords.txt")
	bad_words = f.read().splitlines()
	d = enchant.Dict("en_US")
	d1 = enchant.Dict("en_GB")
	bad=enchant.request_pwl_dict("badwords.txt")
	d2=enchant.request_pwl_dict("adjectives.txt")
	d3=enchant.request_pwl_dict("interject.txt")

	for i in range(0,len(line)):
		
		if(len(line[i])>1):

			w=re.sub(r'([^\w\s]|_)+(?=\s|$)', '', line[i])
			if(len(w)>1):
				
		
				if (d2.check(w)==False and d3.check(w)==False):
			
			
					if(d.check(w) or d1.check(w)):
						
						if(w in bad_words):
							
							list_words.append(i)
							count=count+1
						else:
							if(check_all_combinations(bad_words,w,d,d1)==1):
								list_words.append(i)
								count=count+1
					else:	
						suggest=d.suggest(w);
								
						for word in suggest:
							if(len(word)==w):
								if(word in bad_words):
									
									list_words.append(i)
								
									count=count+1
									break
								else:
									if(check_all_combinations(bad_words,word,d,d1)==1):
										list_words.append(i)
										count=count+1
										break
	
	return count,list_words

#input: list of bad words from dictionary,a word,two dictionary objects
#output: whether bad word or not
def check_all_combinations(bad_words,word,d,d1):

	for i in range(0,len(bad_words)):
		if(d.check(bad_words[i])==False and d1.check(bad_words[i])==False):
			if(len(bad_words[i])==len(word)):
				suggest=d.suggest(bad_words[i])
				
				if(word in suggest):
					return 1
					break		
	
	return 0

	


