import numpy as np

from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_recall_fscore_support

def accu(clf,test_fvec,real_labels):
        predicted = clf.predict(test_fvec.toarray());
        probs = clf.predict_proba(test_fvec.toarray());
        counter=0.0;
        auc_score = roc_auc_score(real_labels, probs[:, 1])
        acc_score = accuracy_score(real_labels, predicted)
        precision,recall,fbeta_score,support = precision_recall_fscore_support(real_labels, predicted, average='micro')
        return acc_score,auc_score,precision,recall,fbeta_score;

def train(labels, fvec):
    clf=RandomForestClassifier(n_estimators=100)
    clf.fit(fvec.toarray(),labels)
    return clf

def test(labels,fvec,test_labels,test_fvec):
    # Training - Random Forest
     clf=train(labels,fvec);
     return accu(clf,test_fvec,test_labels)
