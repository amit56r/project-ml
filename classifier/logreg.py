
from sklearn import linear_model
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support

def train(labels, fvec):
    clf = linear_model.LogisticRegression(tol=1e-8, penalty='l2', C=7)
    clf.fit(fvec, labels)

    return clf


def accu(clf,test_fvec,real_labels):
	predicted = clf.predict(test_fvec);
	probs = clf.predict_proba(test_fvec);
	counter=0.0;
	auc_score = roc_auc_score(real_labels, probs[:, 1])
	acc_score = accuracy_score(real_labels, predicted)
	#print("score: %f (AUC)" % auc_score)
	#print("score: %f (ACC)" % acc_score)
	precision,recall,fbeta_score,support = precision_recall_fscore_support(real_labels, predicted, average='micro')
        return acc_score,auc_score,precision,recall,fbeta_score;


def test(labels,fvec,test_labels,test_fvec):
	clf=train(labels,fvec);
	return accu(clf,test_fvec,test_labels)



