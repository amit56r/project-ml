from sklearn import linear_model
from sklearn.cluster import KMeans
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from numpy  import *
from scipy.sparse import *
import sys
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support




class Klog:

	#class intializer
	def __init__ (self, n_clusters=3,tol=1e-8, penalty='l2', C=7):
		self.n_clusters = n_clusters;
		self.tol=tol;
		self.penalty = penalty;
		self.C =C;

		#now creating the n_cluster+1 learners
		self.weak_clf = [];
		self.good_cluster=[];
		self.strong_clf = linear_model.LogisticRegression(tol=self.tol, penalty=self.penalty, C=self.C)
		self.cluster = KMeans(n_clusters=self.n_clusters,init='k-means++');
		for i in range(self.n_clusters):
			self.weak_clf.append(linear_model.LogisticRegression(tol=self.tol, penalty=self.penalty, C=self.C))
			self.good_cluster.append(True);

		#debug stuff	
		#print len(self.weak_clf);


	#method to fit the whole scheme 	
	def fit(self,fvec,labels):
		#always verbose 

		print "clustering the data"
		self.c_labels = self.cluster.fit_predict(fvec);


		fvec = fvec.toarray(); #we will not use spare reprsentation (to much pain)
		
		print "Traning each clf from ensemble"
		for i in range(self.n_clusters):
			t_labels=[];
			t_fvec = [];

			#seperating data by cluster
			for j in range(len(self.c_labels)):
				if i==self.c_labels[j]:
					t_fvec.append(fvec[j]);
					t_labels.append(labels[j]);


			print "Testing cluster goodness"
			print "Cluster # " + repr(i)+ " size = " + repr(len(t_labels));

			if len(t_labels)<3:
				self.good_cluster[i]=False;
			if sum(t_labels)==len(t_labels) or sum(t_labels)==0:
				self.good_cluster[i]=False;

			if not self.good_cluster[i]:
				print "BAD cluster -- ignoring"
				continue;

			#converting to sparse form
			#t_fvec = csr_matrix(t_fvec); ---- #add time		
			print "Training clf # " + repr(i) + " :"
			self.weak_clf[i].fit(t_fvec, t_labels);

		print "Training done"
		print "Augmenting feature vector"

		super_fvec=[];
		for i in range(len(fvec)):

			temp=[];
			for nclf in range (self.n_clusters):
				if not self.good_cluster[nclf]:
					continue;   #skipping
				temp.extend(self.weak_clf[nclf].predict(fvec[i]));
			
			temp.extend(fvec[i]);
			super_fvec.append(temp);

		#print super_fvec[0]

		print "Training strong classifier"
		self.strong_clf.fit(super_fvec,labels);
		print "Done fitting"


	#fundtion to predict type={deep,cluster}
	def predict(self,fvec, type='deep'):

		fvec = fvec.toarray(); #we will not use spare reprsentation (to much pain)

		if type=='deep':
			print "deep prediction"

			super_fvec=[];
			for i in range(len(fvec)):

				temp=[];
				for nclf in range (self.n_clusters):
					if not self.good_cluster[nclf]:
						continue;   #skipping
					temp.extend(self.weak_clf[nclf].predict(fvec[i]));
			
				temp.extend(fvec[i]);
				super_fvec.append(temp);

			return self.strong_clf.predict(super_fvec);

		if type=='cluster':
			print "cluster prediction"

			predicted=[];
			size = len(fvec);
			cluster_num = self.cluster.predict(fvec);

			for i in range(len(fvec)):
			
				if self.good_cluster[cluster_num[i]]:
					temp = self.weak_clf[cluster_num[i]].predict(fvec[i]);
					predicted.extend(temp)
				else:
					temp=[];
					for nclf in range (self.n_clusters):
						if not self.good_cluster[nclf]:
							continue;   #skipping
						temp.extend(self.weak_clf[nclf].predict(fvec[i]));
			
					temp.extend(fvec[i]);
					temp = self.strong_clf.predict(temp);
					predicted.extend(temp);

				#sys.stdout.write("\r[%d out of %d ]" % (i+1,size))
        		#sys.stdout.flush()

			return predicted;

	#fundtion to predict type={deep,cluster}
	def predict_proba(self,fvec, type='deep'):

		fvec = fvec.toarray(); #we will not use spare reprsentation (to much pain)

		if type=='deep':
			print "deep prediction"

			super_fvec=[];
			for i in range(len(fvec)):

				temp=[];
				for nclf in range (self.n_clusters):
					if not self.good_cluster[nclf]:
						continue;   #skipping
					temp.extend(self.weak_clf[nclf].predict(fvec[i]));
			
				temp.extend(fvec[i]);
				super_fvec.append(temp);

			return self.strong_clf.predict_proba(super_fvec);

		if type=='cluster':
			print "cluster prediction"

			predicted=zeros((len(fvec),2));
			size = len(fvec);
			cluster_num = self.cluster.predict(fvec);

			for i in range(len(fvec)):
			
				if self.good_cluster[cluster_num[i]]:
					temp = self.weak_clf[cluster_num[i]].predict_proba(fvec[i]);
					predicted[i,:]=temp;
				else:
					temp=[];
					for nclf in range (self.n_clusters):
						if not self.good_cluster[nclf]:
							continue;   #skipping
						temp.extend(self.weak_clf[nclf].predict(fvec[i]));
			
					temp.extend(fvec[i]);
					temp = self.strong_clf.predict_proba(temp);
					predicted[i,:]=temp;

				#sys.stdout.write("\r[%d out of %d ]" % (i+1,size))
        		#sys.stdout.flush()

			return predicted;


	#function to test 
	def test(self,labels,fvec,test_labels,test_fvec,type='deep'):
		self.fit(fvec,labels);
		predicted = self.predict(test_fvec,type);
		probs = self.predict_proba(test_fvec,type);

		auc_score = roc_auc_score(test_labels, probs[:, 1])
		acc_score = accuracy_score(test_labels, predicted)

		precision,recall,fbeta_score,support = precision_recall_fscore_support(test_labels, predicted, average='micro')
        	return acc_score,auc_score,precision,recall,fbeta_score;

















		











#test area 
#l=Klog(3);