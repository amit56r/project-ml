

import enchant
import os
import re
import csv
from stemming.porter2 import stem


#Function to find the occurences of pronouns sucn as you you're ...
#inputs: tokenized list of the sentence (in order)
#output: returns list of indexes where pronounc occurs 
def Ufind(line):
	#intial work 
	found_list=[];
	ulist = os.path.join(os.path.dirname(__file__), "ulist.txt")
	f = open(ulist);
	keys = f.read().splitlines();
	d = enchant.DictWithPWL("en_US",ulist);
	#d = enchant.request_pwl_dict(ulist); # to speed up 

	for i in range(len(line)):
		bw=re.sub(r'([^\w\s]|_)+(?=\s|$)', '', line[i]);
		bw=bw.lower();

		if len(bw)==0:  #cannot have empty strings
			continue;

		if d.check(bw):
			if bw in keys:
				found_list.append(i);
				#print line[i]
		else:
			if (bw.find('u')> -1 or bw.find('y')> -1): 
				sug_list = d.suggest(bw);
				sug_list=sug_list[0:5]; #also need to contrain the number of suggestions 
				for w in sug_list:
					if w in keys:
						found_list.append(i);
						#print line[i]
						#print sug_list 
						break;  #can only occur once 

	return found_list;


# Function to measure the distance between the pronouns and bad words
#input: list of indexes where pronouns occurs, list of indexes where bad words occurs.
#output: return the closest distances for every pronoun from the bad words.


def U_bwd_dist(pronoun_index,bwd_index):
     min_dist_list=[]
     max_dist_list=[]
     # checking if the list are empty
     if(len(pronoun_index)==0 or len(bwd_index)==0):
          return min_dist_list, max_dist_list

     # finding closest distances between the pronouns and bad words
     for i in range(len(pronoun_index)):
          min_dist=1e6;
          max_dist=0;
          for j in range(len(bwd_index)):
               dist=abs(pronoun_index[i]-bwd_index[j])
               if(dist < min_dist):
                    min_dist=dist
	       if(dist > max_dist):
                    max_dist=dist

          min_dist_list.append(min_dist)
	  max_dist_list.append(max_dist)
     return min_dist_list, max_dist_list


# Function to check if pronoun and bad words are in a close range(5)
#input: list of indexes where pronouns occurs, list of indexes where bad words occurs.
#output: return a list indicating the distance between the pronouns and the bad word.


def U_dist_vec(pronoun_index,bwd_index):
     dist_list=[0]*5
     min_dist_list=[]
     # checking if the list are empty
     if(len(pronoun_index)==0 or len(bwd_index)==0):
          return dist_list

     # finding closest distances between the pronouns and bad words
     for i in range(len(pronoun_index)):
          min_dist=1e6;
          for j in range(len(bwd_index)):
               dist=abs(pronoun_index[i]-bwd_index[j])
               if(dist < min_dist):
                    min_dist=dist
          min_dist_list.append(min_dist)
     m=min(min_dist_list)
     
     # checking of the minimum distance is less than 5
     if(m>0 and m<=5):
	     dist_list[m-1]=1

     return dist_list




#function to count the number of @
#inputs: tokenized list of the sentence (in order)
#output: returns the count 
def at_count(line):
	count=0;
	for w in line:
		count+=w.count('@');
	return count;

#function to count the number of !
#inputs: tokenized list of the sentence (in order)
#output: returns the count 
def bang_count(line):
	count=0;
	for w in line:
		count+=w.count('!');
	return count;


#Function to find the occurences of bad words
#inputs: tokenized list of the sentence (in order)
#output: returns list of indexes where bad_words occurs 
def bad_find(line):
	#intial work 
	found_list=[];
	badlist = os.path.join(os.path.dirname(__file__), "badwords_full.txt")
	f = open(badlist);
	keys = f.read().lower().splitlines();
	d = enchant.DictWithPWL("en_US",badlist);
	#d = enchant.request_pwl_dict(ulist); # to speed up 
	pcount=0;

	for i in range(len(line)):
		bw=re.sub(r'([^\w\s]|_)+(?=\s|$)', '', line[i])
		bw=bw.lower();

		if len(bw)<=1:
			continue;	#skip for 1 letter words

		 # add suggestes list wordn leng >= to original
		if bw in keys:
			found_list.append(i);
			pcount+=1;
			#print line[i]
		elif stem(bw) in keys:  #checking the stemmed version
			found_list.append(i);
			pcount+=1;

		elif not d.check(bw):
			sug_list = d.suggest(bw);
			sug_list=sug_list[0:5];   #insure that we dont pick really low porbabilty spell sugestions
			for w in sug_list:
				if w in keys:
					found_list.append(i);
					pcount+= 1#1.0/(1.0+sug_list.index(w));
					#print line[i]
					#print sug_list
					#print d.suggest(line[i]) 
					break;  #can only occur once
				elif stem(w) in keys:	#checking the stemed version
					found_list.append(i);
					pcount+= 1#1.0/(1.0+sug_list.index(w));
					break;


	return pcount,found_list;




#test
#p_index=[1,6,17]
#b_index=[25,4,8,9]
#print U_dist_vec(p_index,b_index)
#print Ufind(['i', 'really', "don't", 'understand', 'your', 'point.', 'it', 'seems', 'that', 'you', 'are', 'mixing', 'apples', 'and', 'oranges.'])
#print bang_count(['@blah','and','@blah','is','a','blah!!!'])
#print at_count(['@blah','and','@blah','is','a','blah!!!'])
#test=['shity', 'the', 'fuck', 'up', 'you', 'and', 'ther', 'rest', 'of', 'your', 'faggot', 'friends', 'should', 'be', 'burned', 'at', 'the', 'stake']
#print return_badwords(test)
#print bad_find(test)
#print bad_find(['faggot'])
#print Ufind(['damn,', 'speedy', 'recovery,', 'paul', 'raining', 'prayers', 'down', 'on', 'you!'])

#badlist = os.path.join(os.path.dirname(__file__), "badwords.txt")
#d = enchant.DictWithPWL("en_US",badlist);
#word = '-'
#print d.check(word);
#print d.suggest(word);
