
\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper

\IEEEoverridecommandlockouts                              % This command is only needed if 
                                                          % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed


\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[margin=0.6in]{geometry}
\usepackage{enumerate}
\usepackage{listings}
\usepackage{framed}
\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{caption}
%\usepackage{subcaption}
\usepackage{hyperref}

\DeclareMathOperator{\Tr}{Tr} 
\DeclareMathOperator{\E}{E} 
\DeclareMathOperator{\Var}{Var} 
\DeclareMathOperator{\Cov}{Cov} 
\newcommand{\todo}{\textbf{TODO}}

\title{\LARGE \bf
Insult Detection in Social Media
}


\author{Amit Roy$^{1}$, Nisha Ramesh, Nivedita Viswnath$^{1}$, Sayan Dey$^{1}$% <-this % stops a space
\thanks{$^{1}$School of Computing University of Utah
       {\tt\small \{aroy,nivedita,sayand\}@cs.utah.edu nshramesh@sci.utah.edu}}  
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
Social media being an important part of daily life and being a medium of tremendous amounts of communication and information exchange, requires a certain amount of policing. User comments are a usually riddled with insults,foul language and racial slurs which impede user participation and also degrades the quality of that social media site. This paper describes a machine language approach to detect insults in comments and flag them. Different machine language algorithms were used to be trained on data obtained from Kaggle competition website. Furthermore a custom ensemble method was devised, based on k-mean clustering, that performed nearly as well as the best machine learning algorithm for this problem. The features extracted went through a feature selection algorithm that aims to increase accuracy of the learners and also remove less meaningful features.  The best learner gave us an accuracy of 85\% on our testing data. We also compare our other methods and discuss their pros and cons.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
In recent time, there has been a tremendous increase in online participation on social networking sites, online forums and blogs for the purpose of networking and sharing knowledge. Easy availability of information has rendered almost everyone armchair experts on most topics. With there being so much diversity in opinion, some users may make comments that are considered inappropriate and insulting by others users. Insults can take the form of racial slurs, taunts, abusive or foul language. Insults, harassments and abuse tend to frustrate a lot of people. Furthermore, they act as a barrier to complete user participation.

Most social networking sites and forums today prohibit the use of abusive, unlawful and insulting comments. However, this is only partially enforced, and filtered to a limited extent. Due to the enormous amount of content being posted online, it is impossible to have a human act as a moderator, review the comments being posted and flag any insults that may appear. Thus, the need for an automatic classifier that is fast and effective in filtering out insults that appear in comments. However, there are numerous challenges involved in this process. Some examples are spelling errors: ``Your really dum!'', introduction of spaces: ``s h i t'',  variation of a word: ``you @\$\$h0le'', sarcasm: ``Well, today is a total waste of makeup'', implied insults: ``Take your pills. The voices are taking control again!!''.

Of late, pattern recognition and machine learning algorithms are being used extensively in several Natural Language Processing applications. In this paper, we employ machine learning techniques to detect insults in social media. We obtained our datasets from the Kaggle website (\cite{data}). Our training dataset contains just under 4000 comments. Our testing dataset contains close to 2500 comments. Our primary goal is to predict whether a comment is an insult to one of the participants of the conversation. In addition to this, we explore the possibility of classifying sarcastic comments and implied insults. We then compare the performance different, popular machine learning classification algorithms. We also experiment with the feature vector to understand the important role it plays in different algorithms.

We treat this problem as a binary classification problem. We apply supervised learning models of Logistic Regression, Support Vector Machines (SVM), Random Forests and Adaboost to the datasets. We see the effect of standard feature extraction techniques such as n-grams, td-idf score, stemming and bad word dictionaries  on the accuracy of our models. We employ the ``Chi-Squared Test'' to our feature vector to select the most important features for our learner. We finally show that Logistic Regression gives the highest accuracy amongst all the employed classification algorithms.

The rest of this paper is organized as follows. Section 2 describes the related work on this problem. In Section 3, we explain the feature extraction process we followed. Section 4 contains the feature selection process. Section 5 details the methods we implemented. Section 6 gives a description about our experiments and results. We conclude the paper in Section 7 by summarizing our work and indicating directions for future work.

\section{RELATED WORK}
There have been several attempts at detection of insults in the recent past. A flame recognition system called Smokey, proposed by Ellen Spertus  (\cite{smokey}), looks for syntactic constructs that are condescending and insulting. This is done by parsing and converting each sentence into a Lisp s-expression, which is in turn converted to a feature vector. However, this technique suffers from a high false positive rate. Another approach by Mahmud et. al  (\cite{mahmud}) uses semantic rules to interpret the basic meaning of a sentence  and then decides whether the sentence is information or a 'flame'. However, this approach doesn't check for spelling errors and requires all bad words to exist in a lexicon entry. Moreover, a probabilistic parser is used, which does not guarantee its output to be right. The work of Razavi et. al (\cite{razavi}) rely heavily on bad words dictionaries and cannot function without these lists. The work of P. Goyal and G. Singh  (\cite{iitk}) involves extensive normalization of 
input data before training the classifier. However, their experimentation with classification algorithms is limited.

Furthermore, none of these approaches detect insults that are directed at non-participants of a conversation, such as celebrities, famous personalities, specific person, etc. We aim to develop a classifier that is capable of detecting any kind of insult, and is efficient in doing so.

\section{FEATURE EXTRACTION}
The data set that we are working with provides us with the list of comments and corresponding labels. We need to convert each of the comments to a meaningful feature vector that can be used by each of our machine learning algorithms. In order to convert the given comments to feature vectors , we go through number of stages and use different Natural Language processing techniques to obtain a accurate representation of the comment in feature vector form.

\subsection{Preprocessing}

To classify a comment as insulting we only need to focus on the sentence and each of its words, but the raw comments that we work with contain many misspelling and added punctuation that make the comments unintelligible by machine standards. Hence, the first stage is to  preprocess which removes these unwanted strings,hyphens and punctuations in order to leave us with only the crucial parts of the comment which is then tokenized (fig. \ref{clean}). Note that we take a conservative approach throughout our feature extraction stages in order to minimize information loss.

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.20]{clean.png}
      \caption{Removing unwanted characters and tokenizing }
       \label{clean}
\end{figure}
We defer the spell checking stage when we select the individual features to be part of the feature vector. 

\subsection{Bad word count}

Most comments that are classified as insulting, contained a offensive word.  Frequency of bad words within a comments are taken as one of the features. A dictionary of 1195 bad words is compiled, which also includes variations that is commonly used in social media (e.g @\$\$ - ass or s h i t - shit). The aforementioned dictionary is not exhaustive and hence other techniques are used to identify bad words that cannot be captured by merely using the dictionary. Spellchecking and stemming are employed to capture bad word variations that are not contained within the dictionary. The spellchecker uses the US english dictionary and our bad word dictionary as reference, so that misspelling of neutral words are not misclassified as a insulting word.

Stemming reduces a word to its core root, for example the word ``retarded'' will become ``retard'' after stemming has been applied. Note that stemming is only used in the bad word counting, and it not applied to the actual data in order to avoid information loss.

\subsection{Occurrence of pronouns}

The most common and obvious case of insulting comments contained a pronoun ``you'' followed by a bad word (e.g you ... XXXX). Hence the count of pronoun ''you'' is also included as a feature. Again a small dictionary and a spellchecker is used to capture all variations of ``you'', ''you're'' (e.g u,ur etc). The distance between the pronoun and the bad world is also included as a feature, if there are multiple such instances within a single comment we select the instance with the minimum distance between the pronoun and bad word to be included in the feature vector. Furthermore a pronoun and a bad-word that are separated more than 4 words are ignored, as those are not a good indicator of a insulting comment.

\subsection{Other minor features}

Counts of occurrences of the character \{@,!\} are also part of the feature set. The @ symbol usually indicates a slur directed towards a specific user in the scope of the social media platform. The ! symbol can be seen as emphasis being put by the user to particular parts of the comment. Note these two features are weak indicators for a comment to be insulting. All the features that have been mentioned up till now will be referred as \textit{handcrafted} feature set.
\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.25]{handcraft.png}
      \caption{example of features present in a comment }
       \label{handcraft}
\end{figure}
Figure \ref{handcraft} shows the handcrafted features that are being extracted from each comment.


\subsection{Standard feature extraction techniques}

Standard feature extraction techniques such as n-grams and Tf-idf scoring are employed as they are commonly used in the Natural language processing domain \cite{iitk}.

\subsubsection{Td-idf Score}
Td-idf stand for ``Term frequency $\times$ Inverse document Frequency", it is a weightage assigned to each term that occurs in a comment. The weightage signifies the importance of that term in relation to the original training data. Numerically it is the frequency of a term in the training document divided by the frequency of the term found in an instance of comment (e.g a comment that supposed to be classified by a Machine learning algorithm). So the feature vector will contain possibly many entities that corresponds to each unique term found in training document.

\subsubsection{n-grams}
Can be though of taking $n$ consecutive words and considering that as a single feature or term. N-grams allows us to abstract out the ordering importance in each sentence. Adding n-gram coupled with the Td-idf score will further increase the size of the feature vector, as each n-gram will be treated as a term when calculating the td-idf score. 



\section{FEATURE SELECTION}
We try to approximate a relationship between the input $X={x_1, x_2,...x_M}$ and an output $Y$ using a functional mapping such as $Y=f(X)$. Sometimes the output Y is not determined by the complete set of
 input features ${x_1, x_2, ... ,x_M}$, instead, it is decided only by a subset of them ${x_{(1)}, x_{(2)}, ... , x_{(m)}}$ where $m<M$. It is not always feasible to use all the input features, including
the irrelevant features to approximate the underlying function between the input and output. There maybe constraints on time and data. Additionally, there are two problems that may be foreseen by using
irrelevant features in the learning process.
\begin{itemize}
\item The irrelevant input features will induce greater computational cost.
\item The irrelevant input features may lead to overfitting.
\end{itemize}
Since our goal is to approximate the underlying function between the input and the output, it is reasonable and important to ignore those input features with little effect on the output.\\ \\
In our problem we see that the features generated are of the order of hundred thousands. Not all these features are meaningful, we may have some irrelevant features that maybe overfitting the data.
We need to select the most importance features that play a prominent role in the classification. This is done by using a statistical test known as ``Chi-Squared Test'' to our feature vector. Using 
this test we select the best k number of features. We used $k=2000$ in our experiments.\\

\subsection{Chi-Squared test}
Feature Selection via chi square $(\chi^2)$ test is a very commonly used method. Chi-squared metric  evaluates the cost of a feature by computing the value of the chi-squared statistic 
with respect to the class. The initial hypothesis H0 is the assumption that the two features are unrelated, and it is tested by chi squared formula as is shown in equation (\ref{eq:chi2}).
\begin{equation}
\chi^2 = \sum \frac{(O_{ij} - E_{ij})^2}{E_{ij}}
\label{eq:chi2}
\end{equation}
where $O_{ij}$ is the observed frequency and $E_{ij}$ is the expected frequency, asserted by the null hypothesis.  
Higher the value of $(\chi^2)$, greater the evidence against the hypothesis H0, hence more related is the two variables. Lesser the value of $(\chi^2)$,the hypotheses tends to be true, 
 the variables are independent. 

\section{METHODS}
We used four different machine learning algorithms to learn the modeling function. We used SVM, Logistic Regression, Random Forest, Adaboost. A short summary of these algorithms are given below.

\subsection{Support Vector Machines(SVM)}
SVM are based on mapping the training data into a feature space using the  kernel function and then separating the data using a large margin hyperplane. Intuitively, the kernel computes a similarity
between two given examples. Most commonly used kernel functions are RBF kernels $k(x,x')=exp(\frac{||x-x'||^2}{\sigma^2})$ and polynomial kernel $k(x,x')=(x.x')^d$. SVM is used widely because of a wide variety of kernel selections. SVM finds a large margin separation between the training examples and previously
unseen examples. 
\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.45]{SvmMargin.jpg}
      \caption{Support Vector Machines}
       \label{svm}
\end{figure}


\subsection{Logistic Regression(LOG-REG)}
Logistic Regression involves a more probabilistic view of classification. The probabilities describing the possible outcomes of a single trial are modeled, as a function of the predicted  variables, 
using a logistic function given below.
\begin{equation}
g(\theta)=\frac{1}{1+e^{-\theta}}
\end{equation} 


\subsection{Random Forest(RAN-FOR)}
Random forests are an ensemble learning method for classification that grow many decision trees at training time.
To classify a new object from an input vector, the input vector is fed to each of the trees in the forest. Each tree gives a classification, the output class is mode of the classes output by the individual trees. 

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.3]{randomforest.png}
      \caption{Random Forest Classification}
       \label{rf}
\end{figure}

\subsection{Adaboost(BOOST)}
The basic idea of boosting and ensemble learning algorithms is to iteratively combine relatively simple base hypotheses(weak learners) for the final prediction. In boosting the base hypotheses are linearly combined. In the case of two class classification, the final prediction is the weighted majority of the votes. The combination of these simple rules can boost the performance drastically.
 AdaBoost is adaptive algorithm such that the subsequent classifiers built are in favor of those instances misclassified by previous classifiers. AdaBoost is sensitive to noisy data and outliers.
 It can be less susceptible to the overfitting problem than most learning algorithms.

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.50]{adaboost.png}
      \caption{Adaboost}
       \label{aboost}
\end{figure}

\subsection{Logistic Regression with Clustering}
We used a method combining Logistic Regression and k-means clustering to be able to group similar type of comments so that classifiers can learn that ``rule of thumb'' automatically. In this approach we cluster the data in $k=7$ groups and fit weak sigmoidal functions to each of the clusters. We used k-means algorithm to get the clusters. A cluster is used for training only 
if it has a considerable number of data points, otherwise it is ignored as a bad cluster. Let us assume the number of valid clusters to be $n$. A set of weak classifiers are trained for all 
the valid clusters using the training dataset. The feature vector is augmented with the predictions of each of the weak classifiers for the training dataset to build a super set of the 
feature vector. 
A strong classifier is fit using the super set of the feature vector of the training set. 
This process is repeated for the test data set. We first get predictions from the weak learners as in Figure(\ref{step1}). Using the strong classifier we make predictions for the test dataset as 
seen in Figure(\ref{step2}).

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.50]{step1.png}
      \caption{Predictions from weak classifiers}
       \label{step1}
\end{figure}

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.50]{step2.png}
      \caption{Prediction from strong classifier}
       \label{step2}
\end{figure}


\section{EXPERIMENTS AND RESULTS}
We implement the 5 algorithms described in the METHODS section of this paper using the scikit-learn machine learning library in python.
\subsection{Datasets}
The datasets we use for our experiments are collected from the Kaggle website(\cite{data}).The data consists of a label column followed by two attribute fields.
The first attribute is the time at which the comment was made.The second attribute is the unicode-escaped text of the english language comment,surrounded by double-quotes.
The dataset can be broadly divided into the following parts:
\begin{itemize}
 \item Train dataset-Use primarily for training the algorithms.
 \item Test dataset-Use primarily for testing the algorithms after training.
 \item Verification dataset-Use primarily for testing the algorithms after training.
\end{itemize}
\subsection{Evaluation parameters}
We implement the evaluation parameters for determining the accuracy of the algorithms using the scikit-learn machine learning library in python.
\begin{itemize}
 \item Accuracy score(ACC):In multilabel classification, this function computes subset 
 accuracy i.e. the set of labels predicted for a sample must exactly match the corresponding set of labels in ground truth (correct) labels.
 \item Area under curve score(AUC):Computes the Area Under the Curve (AUC) from prediction scores and this evaluation parameter is strictly restricted to
 binary classification.As our task is to classify the comments as an insult/not insult i.e. a binary classification task, this evaluation parameter is very
 important.
 \item Precision(PREC):The precision is the ratio tp / (tp + fp) where tp is the number of true positives and fp is the number of false positives.
 It is the ability of the classifier not to label a sample as positive that is negative.
 \item Recall(RECALL):The recall is the ratio tp / (tp + fn) where tp is the number of true positives and fn is the number of false negatives.
 It is the ability of the classifier to find all the positive samples.
 \item F-beta score(BETA):It is the weighted harmonic mean of the precision and recall.Its best value is at 1 and worst at 0.
\end{itemize}

\subsection{Approach}
At first, we construct the feature vector taking into account only the bad word count,occurrence of pronouns and other minor features such as occurrences of characters like @ and !.We train our algorithms based on this feature vector
and the best accuracy score we get is of logistic regression which is 77\%.Then, we build a feature vector consisting of the standard feature extraction techniques such as n-grams and TF-idf scores and we train our algorithms based on this feature vector.As a result, the best accuracy that we get is of logistic
regression, which is of 83\%.Then, we include n-grams and TF-idf scores in our previous feature vector to further increase the accuracy of the algorithms.As a result,the best accuracy we get is of logistic regression(84\%) which is a 1\%
increase in the accuracy level.We then implemented the feature selection via the chi square, to get rid of the irrelevant features.Consequently the best accuracy we got are of logistic regression and K means clustering,both giving 85\%.

The following figure(fig. \ref{acc scores of classification algorithms})compares the accuracy scores of the different classification algorithms after introducing the feature selection in the feature vector.

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.40]{chart_1.png}
      \caption{Accuracy scores of different classification algorithms}
       \label{acc scores of classification algorithms}
\end{figure}

Since logistic regression produces the best accuracy out of the 5 algorithms we compare the accuracy level produced by it in the following figure(fig. \ref{feature_vectors}), based on the different types of feature vector we construct during our experiments.

\begin{figure}[thpb]
      \centering
      \includegraphics[scale=0.40]{chart_2.png}
      \caption{Accuracy scores of logistic regression based on different feature vectors}
       \label{feature_vectors}
\end{figure}
\subsection{Results and Analysis}
We train and test our algorithms on different combination of dataset, so that we can be sure of which algorithm performs the best overall and there is no chance of overfitting.The feature vector used in all the
experiments is the feature vector we get after introducing the feature selection via chi square for the feature vector.

The TABLE \ref{table_example1} displays the performance of the algorithms based on different evaluation parameters.We train the algorithms based on the train dataset and test the algorithms on the 
test dataset.
\begin{table}[h]
\caption{Performance of Algorithms train on train dataset and test on test dataset}
\label{table_example1}
\begin{center}
\begin{tabular}{|c||c||c||c||c||c|}
\hline
ALGORITHMS & ACC & AUC & PREC & RECALL & BETA\\
\hline
SVM & 0.84& 0.88& 0.78& 0.58&0.66\\
\hline
LOG-REG & 0.85& 0.88& 0.77& 0.60&0.68\\
 \hline
BOOST & 0.81& 0.82&  0.68& 0.51&0.58\\
 \hline
RAN-FOR & 0.83& 0.88& 0.80& 0.48& 0.60\\
 \hline
CLUST & 0.85& 0.87& 0.77& 0.60&0.68\\
\hline
\end{tabular}
\end{center}
\end{table}
As we can see,logistic regression and Kmeans clustering both perform equally well with respect to accuracy.

The TABLE \ref{table_example2} displays the performance of the algorithms based on different evaluation parameters.We train the algorithms 
based on the train dataset and test the algorithms on the verification dataset.
\begin{table}[h]
\caption{Performance of Algorithms train on train dataset and test on verification dataset}
\label{table_example2}
\begin{center}
\begin{tabular}{|c||c||c||c||c||c|}
\hline
ALGORITHMS & ACC & AUC & PREC & RECALL & BETA\\
\hline
SVM &0.71& 0.79& 0.85& 0.49&0.62\\
\hline
LOG-REG & 0.72& 0.79& 0.84& 0.51&0.64\\
 \hline
BOOST & 0.65& 0.64& 0.74& 0.43&0.54\\
 \hline
RAN-FOR & 0.67& 0.77& 0.83& 0.39&0.53\\
 \hline
CLUST & 0.70& 0.78 & 0.81& 0.50&0.62\\
\hline
\end{tabular}
\end{center}
\end{table}
For this experiment logistic regression performs the best followed by SVM with respect to accuracy.

The TABLE \ref{table_example3} displays the performance of the algorithms based on different evaluation parameters.Here, we train the algorithms 
based on a dataset which is a combination of train and verification datasets and test the algorithms on the test dataset.
\begin{table}[h]
\caption{Performance of Algorithms train on train and verification dataset and test on test dataset}
\label{table_example3}
\begin{center}
\begin{tabular}{|c||c||c||c||c||c|}
\hline
ALGORITHMS & ACC & AUC & PREC & RECALL & BETA\\
\hline
SVM & 0.85& 0.89& 0.75& 0.64&0.69\\
\hline
LOG-REG & 0.85& 0.89& 0.75& 0.66&0.70\\
 \hline
BOOST & 0.80& 0.83& 0.66& 0.55&0.60\\
 \hline
RAN-FOR & 0.84& 0.88& 0.75& 0.59&0.66\\
 \hline
CLUST & 0.85& 0.88& 0.75& 0.67&0.71\\
\hline
\end{tabular}
\end{center}
\end{table}

Here the standout winners were Logistic regression,SVM and clustering with respect to accuracy.

The TABLE \ref{table_example4} displays the performance of the algorithms based on different evaluation parameters.For this experiment,we train the algorithms 
based on a dataset which is a combination of train and test datasets and test the algorithms on the verification dataset.
\begin{table}[h]
\caption{Performance of Algorithms train on train and test dataset and test on verification dataset}
\label{table_example4}
\begin{center}
\begin{tabular}{|c||c||c||c||c||c|}
\hline
ALGORITHMS & ACC & AUC & PREC & RECALL & BETA\\
\hline
SVM & 0.71& 0.80& 0.86& 0.48&0.62\\
\hline
LOG-REG & 0.72& 0.80& 0.84& 0.52&0.64\\
 \hline
BOOST & 0.63& 0.65& 0.75& 0.36&0.49\\
 \hline
RAN-FOR & 0.68& 0.78& 0.84& 0.42&0.56\\
 \hline
CLUST & 0.72& 0.79&0.82&0.53&0.64\\
\hline
\end{tabular}
\end{center}
\end{table}

Here,Logistic regression and clustering both performs best equally with respect to accuracy.

As a result, we can say that logistic regression performs the best consistently for our datasets ahead of all the other algorithms closely followed by k means clustering.
We also try to avoid the problem of overfitting by training the algorithms on different combinations of dataset and consequently testing it on different
combination of datasets too.
\section{CONCLUSIONS}
We developed and implemented an algorithm for detection of insults in social media. We extracted meaningful features from the data and evaluated the most relevant features.
We used four machine learning algorithms to test the performance of different classifiers. We also added a clustering approach with logistic regression to make use of multi-stage classification.
Logistic Regression with k-means clustering seemed to work well for our dataset. \\
For future work, the clustering approach could be combined with other algorithms like SVM, Random Forest. We could also combine
Logistic Regression, SVM, Random Forests in a boosting algorithm. More features could be added to detect implied/sarcastic comments.







\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





\begin{thebibliography}{99}

\bibitem{data} Dataset Url: \url{http://www.kaggle.com/c/detecting-insults-in-social-commentary/data}.
\bibitem{smokey} Ellen Spertus, Smokey: Automatic recognition of hostile messages. Ninth Conference on Innovative Applications of Artificial Intelligence, 1997.
\bibitem{mahmud} Altaf Mahmud, Kazi Zubair Ahmed, Mumut Khan, Detecting flames and insults in text. Sixth International Conference on Natural Language Processing, 2008
\bibitem{razavi} Amir H. Razavi, Diana Inkpen, Sasha Uritsky, and Stan Matwin, Offensive language detection using multi-level classification. 23rd Canadian Conference on Artificial Intelligence, 2010
\bibitem{iitk} P. Goyal,G. S. Kalra , Peer-to-Peer Insult Detection in Online Communities. IITK, unpublished.
\bibitem{scikit} Scikit-learn machine learning library in python.Url: \url{http://scikit-learn.org/stable/}.


\end{thebibliography}




\end{document}
