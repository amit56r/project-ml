#function to pre-process the data file and tokenize it
#inputs: file name
#output: list of comments tokenized into words, list of labels for each sentence
import re
import csv
import sys
import ast
from feature_selection import features



def tokenizeLabelsAndComments(filename,label_pos=0,comment_pos=2):
	reader = csv.reader(open(filename,"rb"),delimiter=",")
    
	test_data = []
	count = 1
	for row in reader:
        	if count > 1:      
            		test_data.append(row)
        	count += 1
  
    	labels = []
    	comments = []
    	for i in range(0, len(test_data)):
		labels.append(test_data[i][label_pos])
		comments.append(test_data[i][comment_pos])

    	comments = [x.lower() for x in comments]
    	comments = [x.replace("\\n"," ") for x in comments]        
    	comments = [x.replace("\\t"," ") for x in comments]        
    	comments = [x.replace("\\xa0"," ") for x in comments]
    	comments = [x.replace("\\xc2"," ") for x in comments]
    	comments = [x.replace("\\xc8"," ") for x in comments]
    	comments = [x.replace("\\x99"," ") for x in comments]
    	comments = [x.replace("\\"," ") for x in comments]
    	comments = [x.replace("\"", "") for x in comments]
    	comments = [x.replace(" !", "!") for x in comments]
        comments = [x.replace(".", " ") for x in comments]
        comments = [x.replace("-", " ") for x in comments]
        comments = [x.replace("_", " ") for x in comments]

	words = []
	for line in comments:
		temp = line.split()
		words.append(temp)

	return labels, words


#function to create the feature set , with the labels
#input: filename
#ouput: writes to csv (format in accordance to sci-kitlearn)
def write_feature_set(filename_in,filename_out,label_pos=0,comment_pos=2):
    labels,words = tokenizeLabelsAndComments(filename_in,label_pos,comment_pos);

    #words=words[0:1000]
    fvec=[];
    size = len(words);
    counter=1;
    for w in words:
        #features
        word_count = len(w);
        u_index = features.Ufind(w);
        u_count = len(u_index)
        at_count = features.at_count(w);
        bang_count=features.bang_count(w);
        bad_count, bad_index=features.bad_find(w);
        min_dist,max_dist = features.U_bwd_dist(u_index,bad_index);
        dist_vec = features.U_dist_vec(u_index,bad_index)

        if len(min_dist)==0:
            min_dist=[-1]

        if len(max_dist)==0:
            max_dist=[-1];

        #ftemp = [bad_count/float(word_count),u_count,bad_count,min(min_dist)]; SVM ~0.77
        #ftemp = [u_count,bad_count,min(min_dist),bang_count,at_count]; # SVM ~0.77
        ftemp = [word_count,u_count,bad_count,bang_count,at_count];
        ftemp.extend(dist_vec);
        fvec.append(ftemp);

        sys.stdout.write("\r[%d out of %d ]" % (counter,size))
        sys.stdout.flush()

        counter+=1;

    print '\n'
    data_file = open(filename_out,'wb')
    writer = csv.writer(data_file,delimiter = '\t',dialect = 'excel-tab')
    header = ['Label','fvec','Comment']
    writer.writerow(header);
    for i in range(0,len(words)):
        writer.writerow([labels[i], fvec[i],words[i]]);
   

#function to read the processed data found in csv format
#input file name 
def get_data(filename):
    reader = csv.reader(open(filename,"rb"),delimiter="\t")

    test_data = []
    count = 1
    for row in reader:
            if count > 1:      
                    test_data.append(row)
            count += 1

    labels = []
    fvec = []
    words=[]
    for i in range(0, len(test_data)):
        labels.append(ast.literal_eval(test_data[i][0]))
        fvec.append(ast.literal_eval(test_data[i][1]))
        words.append(ast.literal_eval(test_data[i][2]))

    return labels,fvec,words

#function to return the joined version of the word
#need to use with sci-kitlearn standar feature extraction subroutines
def get_joined(words):
    corpus=[];

    for w in words:
        temp = ' '.join(w);
        corpus.append(temp);
    return corpus





#tests
#write_feature_set('data/train.csv','test_init.csv')
#labels,fvec,words=get_data('test.csv')
#fvec,labels = get_feature_set('data/train.csv')

#n=3;
#print words[n]
#print fvec[n]
#labels,fvec,words=get_data('train_init.csv')
#a = get_joined(words);
#print a[6:10]


